import java.util.ArrayList;
import java.util.List;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.swing.JOptionPane;

public class Main {
	
	String fileName;
	
	PrintWriter writer;
    List<String> output;
	
	public Main() throws IOException{
		
		fileName = (String)JOptionPane.showInputDialog("Enter File Name To Convert...");
        if(fileName == null)
            return;
		
		writer = new PrintWriter(fileName + ".txt");
		output = new ArrayList<String>();
        
        BufferedReader reader = new BufferedReader(new FileReader("src/" + fileName + ".txt"));
        String line;
        
        while((line = reader.readLine()) != null){
            decode(line);
        }
        reader.close();
        
        int len = output.size();
        for(int i = 0; i < len; i++){
            writer.println(output.get(i));
        }
        writer.close();
	}

	private void decode(String line) {
		try {
			byte[] bytes = line.getBytes("UTF8");
            String hexData = bytesToHex(bytes);
            int len = hexData.length();

            if(len % 8 != 0){
                int r = len % 8;
                switch(r){
                    case 2:
                        hexData += "000000";
                        break;
                    case 4:
                        hexData += "0000";
                        break;
                    case 6:
                        hexData += "00";
                        break;
                }
            }
            len = hexData.length();

            for(int i = 0; i < len/8; i++){
                output.add(hexData.substring(i*8, i*8+8));
            }
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}	
	}

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    private String bytesToHex(byte[] bytes){
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

	/**
	 * @param args
	 */
	public static void main(String[] args) {
        try {
            Main main = new Main();
        }catch(IOException e){
            e.printStackTrace();
        }
	}

}
