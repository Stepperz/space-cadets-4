run(){
    while finished == 0 {
        svar advanceSearch 0;
        getNextChar();
        if hasKeyWord == 0 {

        }
        if hasName == 0 {

        }
        if advanceSearch == 1 {

        }
    }
}

getKeyWord(){
    rtrgid currentAddress r5;
    rgtr r5 keyWord[0];
    currentAddress = currentAddress + 1;

    rtrgid currentAddress r5;
    rgtr r5 keyWord[1];
    currentAddress = currentAddress + 1;

    rtrgid currentAddress r5;
    rgtr r5 keyWord[2];
    currentAddress = currentAddress + 1;

    rtrgid currentAddress r5;
    rgtr r5 keyWord[3];
    currentAddress = currentAddress + 1;

    rtrgid currentAddress r5;
    rgtr r5 keyWord[4];
    currentAddress = currentAddress + 1;

    if keyWord[4] == 32 {
        currentAddress = currentAddress - 1;
    }
}

//Puts the next line in the line array.
getNextLine(){
    svar looping 1;
    svar lineIndex 0;
    lineIndex = 0;
    while looping == 1 {
        rtrgid currentAddress r5;
        currentAddress = currentAddress + 1;
        rgtr r5 currentChar;
        if currentChar == 59 {
            looping = 0;
        }
    }
}

//Returns the next char to r5 (random register tbh)
getNextChar(){
    rtrgid currentAddress r5;
    currentAddress = currentAddress + 1;
}

main(){

//
//Variable Initialisation--------------------------------------------
//
    svar currentChar 0;

    svar programStartAddress 10000;
    svar currentAddress 10000;
    svar currentScopeDepth 0;

    array keyWord 5;
    array variableName 40;
    array line 120;
    svar variableLength 0;

    svar hasKeyWord 0;
    svar hasName 1;

    svar numVariables 0;
    array variableReferences 50;
    array variableHeap 2100;

    svar finished 0;

    svar lineNumber 0;

    svar zero 0;
    svar one 1;


//create the array containing the 'clear' string in utf-8
    array clear 5;
    clear[0] = 99;
    clear[1] = 108;
    clear[2] = 101;
    clear[3] = 97;
    clear[4] = 114;

//create the array containing the 'incr' string in utf-8
    array incr 4;
    incr[0] = 105;
    incr[1] = 110;
    incr[2] = 99;
    incr[3] = 114;

//create the array containing the 'decr' string in utf-8
    array incr 4;
    incr[0] = 100;
    incr[1] = 101;
    incr[2] = 99;
    incr[3] = 114;

//create the array containing the 'while' string in utf-8
    array while 5;
    while[0] = 119;
    while[1] = 104;
    while[2] = 100;
    while[3] = 108;
    while[4] = 101;

//create the array containing the 'not' string in utf-8
    array not 3;
    not[0] = 110;
    not[1] = 111;
    not[2] = 116;

//create the array containing the 'do' string in utf-8
    array do 2;
    do[0] = 100;
    do[1] = 111;

//create the array containing the 'end' string in utf-8
    array end 3;
    end[0] = 101;
    end[1] = 110;
    end[2] = 100;

//
//---------------------------------------------------------------------
//

    run();

    hlt;
}