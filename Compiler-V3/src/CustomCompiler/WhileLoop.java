package CustomCompiler;

import Shared.Branch;

import java.lang.*;

/**
 * Created by Ollie on 25/10/2014.
 */
public class WhileLoop extends Branch {

    private enum type{
        VAR_VAR,
        VAR_CONST,
        REG_REG
    };
    private type loopType;

    public Variable c1, c2;
    public String constant;
    public String registersToUse;
    public String compareFunc;

    public WhileLoop(Variable c1, String compareFunc, Variable c2){
        this.c1 = c1;
        this.c2 = c2;
        this.compareFunc = compareFunc;
        loopType = type.VAR_VAR;
    }

    public WhileLoop(Variable c1, String compareFunc, String constant){
        this.c1 = c1;
        this.constant = constant;
        this.compareFunc = compareFunc;
        loopType = type.VAR_CONST;
    }

    public WhileLoop(String registers, String compareFunc){
        this.registersToUse = registers;
        this.compareFunc = compareFunc;
        loopType = type.REG_REG;
    }

    @Override
    public void addStartBranchOperations(int startLine){
        this.startLine = startLine;
        switch (loopType){
            case VAR_VAR:
                addInstruction("1600"+c1.hexAddress);
                addInstruction("1610"+c2.hexAddress);
                break;
            case VAR_CONST:
                addInstruction("1600"+c1.hexAddress);
                addInstruction("01"+constant+"01");
                break;
            case REG_REG:
                break;
        }
        addInstruction("WHILE");//Insert jump instruction at startLine
    }

    @Override
    public void addFinalBranchOperations(){
        addInstruction("1100" + Compiler.toHex(startLine));
        switch (loopType){
            case VAR_VAR:
                switch(compareFunc) {
                    case "==":
                        setInnerInstruction(2, "1201" + Compiler.toHex(startLine + numLines));
                        break;
                    case "<=":
                        setInnerInstruction(2, "1401" + Compiler.toHex(startLine + numLines));
                        break;
                    case ">=":
                        setInnerInstruction(2, "1301" + Compiler.toHex(startLine + numLines));
                        break;
                }
                break;
            case VAR_CONST:
                switch(compareFunc) {
                    case "==":
                        setInnerInstruction(2, "1201" + Compiler.toHex(startLine + numLines));
                        break;
                    case "<=":
                        setInnerInstruction(2, "1401" + Compiler.toHex(startLine + numLines));
                        break;
                    case ">=":
                        setInnerInstruction(2, "1301" + Compiler.toHex(startLine + numLines));
                        break;
                }
                break;
            case REG_REG:
                switch(compareFunc) {
                    case "==":
                        setInnerInstruction(0, "12" + registersToUse + Compiler.toHex(startLine + numLines));
                        break;
                    case "<=":
                        setInnerInstruction(0, "14" + registersToUse + Compiler.toHex(startLine + numLines));
                        break;
                    case ">=":
                        setInnerInstruction(0, "13" + registersToUse + Compiler.toHex(startLine + numLines));
                        break;
                }
                break;
        }
    }
}
