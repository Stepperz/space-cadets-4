package CustomCompiler;

/**
 * Created by Ollie on 23/10/2014.
 */
public class Method {

    public String name;
    public JumpPoint startPoint;
    public int numParameters = 0;

    public Method(String name, int startLine, int numParameters){
        this.name = name;
        startPoint = new JumpPoint(startLine);
        this.numParameters = numParameters;
    }

}
