package CustomCompiler;

import java.lang.*;

/**
 * Created by Ollie on 23/10/2014.
 */
public class JumpPoint {

    public String hexPos;

    public JumpPoint(int pos){
        hexPos = Compiler.toHex(pos);
    }

}
