package CustomCompiler;

/**
 * Created by Ollie on 23/10/2014.
 */
public class Variable {

    public String name;
    public String hexAddress;

    public Variable(String name, String hexAddress){
        this.name = name;
        this.hexAddress = hexAddress;
    }

}
