package CustomCompiler;

import Shared.Branch;
import Shared.OutputWriter;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Ollie on 23/10/2014.
 *
 *
 * Method return data is stored in 'r7' whether reference or primitive.
 */
public class Compiler {

    int srcLineNumber = 1;
    int afterSetupPos = 0;
    String to;
    String from;
    String[] parts;

    OutputWriter output;

    ArrayList<Method> methods;
    ArrayList<Variable> variables;
    ArrayList<Array> arrays;
    ArrayList<Branch> branches;

    Method mainMethod = null;

    String fileName = "NULL";

    //Stack tracking
    int currentCallStackSize = 0;
    int currentRegStackSize = 0;

    //TODO: GUI Stuff
    //TODO: Arrays
    //TODO: Dynamic variable creation //In Progress (Efficiency may be a problem)
    //TODO: Value return
    //TODO: Parameters

    public Compiler() throws FileNotFoundException {
        fileName = JOptionPane.showInputDialog("Enter File Name To Compile...");

        parts = new String[4];

        output = new OutputWriter();
        methods = new ArrayList<>();
        variables = new ArrayList<>();
        arrays = new ArrayList<>();
        branches = new ArrayList<>();
    }

    //Called to start the compiler running
    public void run() throws IOException {
        if(fileName == null)
            return;

        formStackSetup();

        variablePass();
        mainPass();

        output.writeToFile(fileName + ".txt");

        JOptionPane.showMessageDialog(null, "Compilation Complete");
    }

    private void mainPass() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("src/" + fileName + ".ls"));
        String line;
        boolean searchingForMethod = true;
        srcLineNumber = 1;

        int numParams;
        String[] params;

        while((line = reader.readLine()) != null) {
            if (!line.startsWith("//")) {
                if (searchingForMethod) {
                    if (line.contains("){")) {
                        params = line.split("\\(")[1].split("\\)")[0].split(",");

                        if (!params[0].trim().isEmpty())
                            numParams = params.length;
                        else
                            numParams = 0;

                        Method m = new Method(line.split("\\(")[0], output.getOutputLineNumber(), numParams);
                        if (m.name.equals("main"))
                            mainMethod = m;

                        methods.add(m);
                        searchingForMethod = false;
                        srcLineNumber++;
                        continue;
                    }
                    processInstruction(line);
                } else {
                    if (!line.contains("}")) {
                        processInstruction(line);
                    } else if (output.branchDepth > 0) {
                        output.closeCurrentBranch();
                    } else {
                        //When the main method has finished, output r0 and halt.
                        if (methods.get(methods.size() - 1) == mainMethod) {
                            output.addToOutput("17000000");
                            output.addToOutput("18000000");
                            System.out.println("INFO: Methods placed after the 'main()' method will not be added.");
                            break;
                        }
                        formMethodReturn();
                        searchingForMethod = true;
                    }
                }
            }
            srcLineNumber++;
        }

        //Add a jump statement to 'main()' at the start of the program.
        if(mainMethod == null) {
            reader.close();
            System.err.println("Error: could not find method 'main'");
            return;
        }

        output.setOutput(afterSetupPos, "1100" + mainMethod.startPoint.hexPos);

        reader.close();
    }

    //Variables populate from ad.65210 and decreases to lower addresses.
    //There is no checking for overwriting a program so be aware of that.
    //Variables are not created at runtime only given pre designated memory addresses.
    private void variablePass() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("src/" + fileName + ".ls"));
        String line;
        String[] parts;
        srcLineNumber = 1;
        int currentAddress = 65210;

        while((line = reader.readLine()) != null){
            line = line.trim();
            if(line.startsWith("svar")) {
                parts = line.split(";")[0].split(" ");
                if(parts.length == 3){
                    Variable v = new Variable(parts[1], toHex(currentAddress));
                    variables.add(v);
                    addVariableInitialisation(v, toHex(parts[2]));
                    currentAddress--;
                }else{
                    System.err.println("Error: Variable cannot be initialised at line " + srcLineNumber);
                }
            }else if(line.startsWith("array")) {
                parts = line.split(";")[0].split(" ");
                if(parts.length == 3){
                    Array v = new Array(parts[1], currentAddress, Integer.parseInt(parts[2]));
                    arrays.add(v);
                    currentAddress -= Integer.parseInt(parts[2]);
                }else{
                    System.err.println("Error: Array cannot be initialised at line " + srcLineNumber);
                }
            }
            srcLineNumber++;
        }

        reader.close();

        //Assign the address for the 'main()' jump and increase the outputLine to accomodate it.
        afterSetupPos = output.getOutputLineNumber();
        output.addToOutput("MAINJUMP");
    }

    //Adds variable initialisation code to the top of the file.
    private void addVariableInitialisation(Variable v, String value){
        output.addToOutput("01" + value + "00");
        output.addToOutput("1500" + v.hexAddress);
    }

    private void processInstruction(String line){
        line = line.trim();

        if(!line.isEmpty()) {
            if (!line.endsWith(";") && !line.endsWith("{")) {
                System.err.println("Error: missing ';' at line " + srcLineNumber);
                return;
            }

            if (line.endsWith(");")) {
                line = line.split(";")[0];
                formMethodCall(line);
                return;
            }

            line = line.split(";")[0];

            parts = line.split(" ");
            Variable v1 = null, v2 = null, v3 = null;

            String s;
            switch (parts[0]) {
                default:
                    if (parts[0].equals("svar") || parts[0].equals("array"))
                        return;
                    for (Variable v : variables) {
                        if (parts[0].equals(v.name) && parts[1].equals("=")) {
                            v1 = v;
                            break;
                        }
                    }
                    if (v1 == null) {
                        if (parts[0].endsWith("]")) {
                            for (Array a : arrays) {
                                if (parts[0].split("\\[")[0].equals(a.name))
                                    v1 = new Variable(null, a.get(Integer.parseInt(parts[0].split("\\[")[1].split("]")[0])));
                            }
                        } else {
                            System.err.println("Error: Cannot find variable with name '" + parts[0] + "' on line " + srcLineNumber);
                            break;
                        }
                    }
                    for (Variable v : variables) {
                        if (parts[2].equals(v.name)) {
                            v2 = v;
                            break;
                        }
                    }
                    if (v2 == null) {
                        if (parts[2].endsWith("]")) {
                            for (Array a : arrays) {
                                if (parts[2].split("\\[")[0].equals(a.name))
                                    v1 = new Variable(null, a.get(Integer.parseInt(parts[2].split("\\[")[1].split("]")[0])));
                            }
                        }
                    }
                    //Check for assignment operation
                    if (parts.length == 3) {
                        if (v2 != null) {
                            if (parts[1].equals("="))
                                output.addToOutput(new String[]{
                                        "1600" + v1.hexAddress,
                                        "1610" + v2.hexAddress,
                                        "02000001",
                                        "1500" + v1.hexAddress
                                });
                        } else {
                            if (parts[1].equals("="))
                                output.addToOutput(new String[]{
                                        "1600" + v1.hexAddress,
                                        "01" + toHex(parts[2]) + "01",
                                        "02000001",
                                        "1500" + v1.hexAddress
                                });
                        }
                        return;
                    }

                    for (Variable v : variables) {
                        if (parts[4].equals(v.name)) {
                            v3 = v;
                            break;
                        }
                    }
                    if (v3 == null) {
                        if (parts[4].endsWith("]")) {
                            for (Array a : arrays) {
                                if (parts[4].split("[")[0].equals(a.name))
                                    v1 = new Variable(null, a.get(Integer.parseInt(parts[4].split("[")[1].split("]")[0])));
                            }
                        }
                    }
                    if (v2 == null && v3 != null) {
                        String c2 = toHex(parts[2]);
                        if (c2 == null) {
                            System.err.println("Error: Cannot find variable with name '" + parts[2] + "' on line " + srcLineNumber);
                        } else {
                            switch (parts[3]) {
                                case ("+"):
                                    output.addToOutput(new String[]{
                                            "1600" + v1.hexAddress,
                                            "01" + c2 + "01",
                                            "1620" + v3.hexAddress,
                                            "03120000",
                                            "1500" + v1.hexAddress});
                                    break;
                                case ("-"):
                                    output.addToOutput(new String[]{
                                            "1600" + v1.hexAddress,
                                            "01" + c2 + "01",
                                            "1620" + v3.hexAddress,
                                            "04120000",
                                            "1500" + v1.hexAddress});
                                    break;
                                case ("*"):
                                    output.addToOutput(new String[]{
                                            "1600" + v1.hexAddress,
                                            "01" + c2 + "01",
                                            "1620" + v3.hexAddress,
                                            "05120000",
                                            "1500" + v1.hexAddress});
                                    break;
                                case ("/"):
                                    output.addToOutput(new String[]{
                                            "1600" + v1.hexAddress,
                                            "01" + c2 + "01",
                                            "1620" + v3.hexAddress,
                                            "06120000",
                                            "1500" + v1.hexAddress});
                                    break;
                            }
                        }
                        break;
                    } else if (v3 == null && v2 != null) {
                        String c2 = toHex(parts[4]);
                        if (c2 == null) {
                            System.err.println("Error: Cannot find variable with name '" + parts[2] + "' on line " + srcLineNumber);
                        } else {
                            switch (parts[3]) {
                                case ("+"):
                                    output.addToOutput(new String[]{
                                            "1600" + v1.hexAddress,
                                            "1610" + v2.hexAddress,
                                            "01" + c2 + "02",
                                            "03120000",
                                            "1500" + v1.hexAddress});
                                    break;
                                case ("-"):
                                    output.addToOutput(new String[]{
                                            "1600" + v1.hexAddress,
                                            "1610" + v2.hexAddress,
                                            "01" + c2 + "02",
                                            "04120000",
                                            "1500" + v1.hexAddress});
                                    break;
                                case ("*"):
                                    output.addToOutput(new String[]{
                                            "1600" + v1.hexAddress,
                                            "1610" + v2.hexAddress,
                                            "01" + c2 + "02",
                                            "05120000",
                                            "1500" + v1.hexAddress});
                                    break;
                                case ("/"):
                                    output.addToOutput(new String[]{
                                            "1600" + v1.hexAddress,
                                            "1610" + v2.hexAddress,
                                            "01" + c2 + "02",
                                            "06120000",
                                            "1500" + v1.hexAddress});
                                    break;
                            }
                        }
                        break;
                    } else if (v2 != null && v3 != null) {
                        switch (parts[3]) {
                            case ("+"):
                                output.addToOutput(new String[]{
                                        "1600" + v1.hexAddress,
                                        "1610" + v2.hexAddress,
                                        "1620" + v3.hexAddress,
                                        "03120000",
                                        "1500" + v1.hexAddress});
                                break;
                            case ("-"):
                                output.addToOutput(new String[]{
                                        "1600" + v1.hexAddress,
                                        "1610" + v2.hexAddress,
                                        "1620" + v3.hexAddress,
                                        "04120000",
                                        "1500" + v1.hexAddress});
                                break;
                            case ("*"):
                                output.addToOutput(new String[]{
                                        "1600" + v1.hexAddress,
                                        "1610" + v2.hexAddress,
                                        "1620" + v3.hexAddress,
                                        "05120000",
                                        "1500" + v1.hexAddress});
                                break;
                            case ("/"):
                                output.addToOutput(new String[]{
                                        "1600" + v1.hexAddress,
                                        "1610" + v2.hexAddress,
                                        "1620" + v3.hexAddress,
                                        "06120000",
                                        "1500" + v1.hexAddress});
                                break;
                        }
                    }
                    break;

                case "ldi":
                    switch (parts[1]) {
                        case ("r0"):
                            s = toHex(parts[2]);
                            output.addToOutput("01" + s + "00");
                            break;
                        case ("r1"):
                            s = toHex(parts[2]);
                            output.addToOutput("01" + s + "01");
                            break;
                        case ("r2"):
                            s = toHex(parts[2]);
                            output.addToOutput("01" + s + "02");
                            break;
                        case ("r3"):
                            s = toHex(parts[2]);
                            output.addToOutput("01" + s + "03");
                            break;
                        case ("r4"):
                            s = toHex(parts[2]);
                            output.addToOutput("01" + s + "04");
                            break;
                        case ("r5"):
                            s = toHex(parts[2]);
                            output.addToOutput("01" + s + "05");
                            break;
                        case ("r6"):
                            s = toHex(parts[2]);
                            output.addToOutput("01" + s + "06");
                            break;
                        case ("r7"):
                            s = toHex(parts[2]);
                            output.addToOutput("01" + s + "07");
                            break;
                    }
                    break;

                case "mvr":
                    if (parts[1].equals("r0"))
                        from = "0";
                    if (parts[1].equals("r1"))
                        from = "1";
                    if (parts[1].equals("r2"))
                        from = "2";
                    if (parts[1].equals("r3"))
                        from = "3";
                    if (parts[1].equals("r4"))
                        from = "4";
                    if (parts[1].equals("r5"))
                        from = "5";
                    if (parts[1].equals("r6"))
                        from = "6";
                    if (parts[1].equals("r7"))
                        from = "7";
                    if (parts[2].equals("r0"))
                        to = "0";
                    if (parts[2].equals("r1"))
                        to = "1";
                    if (parts[2].equals("r2"))
                        to = "2";
                    if (parts[2].equals("r3"))
                        to = "3";
                    if (parts[2].equals("r4"))
                        to = "4";
                    if (parts[2].equals("r5"))
                        to = "5";
                    if (parts[2].equals("r6"))
                        to = "6";
                    if (parts[2].equals("r7"))
                        to = "7";
                    output.addToOutput(("020000" + to + from));
                    break;

                case "add":
                    output.addToOutput("03" + toHex(parts[1]).substring(3) + toHex(parts[2]).substring(3) + "000" + toHex(parts[3]).substring(3));
                    break;
                case "sub":
                    output.addToOutput("04" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));
                    break;
                case "mul":
                    output.addToOutput("05" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));
                    break;
                case "div":
                    output.addToOutput("06" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));
                    break;
                case "neg":
                    output.addToOutput("07" + toHex(parts[1]) + "0000" + toHex(parts[2]));
                    break;
                case "rsft":
                    output.addToOutput("08" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));
                    break;
                case "iva":
                    output.addToOutput("09" + toHex(parts[1]) + "0000" + toHex(parts[2]));
                    break;
                case "ivb":
                    output.addToOutput("0a" + toHex(parts[1]) + "0000" + toHex(parts[2]));
                    break;
                case "and":
                    output.addToOutput("0b" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));
                    break;
                case "nand":
                    output.addToOutput("0c" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));
                    break;
                case "or":
                    output.addToOutput("0d" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));
                    break;
                case "nor":
                    output.addToOutput("0e" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));
                    break;
                case "xor":
                    output.addToOutput("0f" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));
                    break;
                case "xnor":
                    output.addToOutput("10" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));
                    break;

                case "rgtr":
                    if (parts[1].equals("r0"))
                        from = "0";
                    if (parts[1].equals("r1"))
                        from = "1";
                    if (parts[1].equals("r2"))
                        from = "2";
                    if (parts[1].equals("r3"))
                        from = "3";
                    if (parts[1].equals("r4"))
                        from = "4";
                    if (parts[1].equals("r5"))
                        from = "5";
                    if (parts[1].equals("r6"))
                        from = "6";
                    if (parts[1].equals("r7"))
                        from = "7";
                    if (!canParse(parts[2])){
                        for (Variable v : variables) {
                            if (parts[2].equals(v.name)) {
                                v1 = v;
                                break;
                            }
                        }
                        if(v1 == null) {
                            if (parts[2].endsWith("]")) {
                                for (Array a : arrays) {
                                    if (parts[2].split("[")[0].equals(a.name)){
                                        v1 = new Variable(null, a.get(Integer.parseInt(parts[2].split("[")[1].split("]")[0])));
                                        break;
                                    }
                                }
                            }
                        }
                        if (v1 == null) {
                            System.err.println("Error: Cannot find variable with name '" + parts[2] + "' on line " + srcLineNumber);
                            break;
                        }
                        output.addToOutput("15" + from + "0" + v1.hexAddress);
                    }else {
                        output.addToOutput("15" + from + "0" + toHex(parts[2]));
                    }
                    break;

                //Indirect causes the register value to be written to the ram address stored in the variable given.
                //This uses r7 do not use r7!!
                case "rgtrid":
                    if (parts[1].equals("r0"))
                        from = "0";
                    if (parts[1].equals("r1"))
                        from = "1";
                    if (parts[1].equals("r2"))
                        from = "2";
                    if (parts[1].equals("r3"))
                        from = "3";
                    if (parts[1].equals("r4"))
                        from = "4";
                    if (parts[1].equals("r5"))
                        from = "5";
                    if (parts[1].equals("r6"))
                        from = "6";
                    if (parts[1].equals("r7")) {
                        System.err.println("Error: r7 is in use in 'rgtrid' cannot use r7 as parameter on line " + srcLineNumber);
                        break;
                    }
                    if (!canParse(parts[2])){
                        for (Variable v : variables) {
                            if (parts[2].equals(v.name)) {
                                v1 = v;
                                break;
                            }
                        }
                        if(v1 == null) {
                            if (parts[2].endsWith("]")) {
                                for (Array a : arrays) {
                                    if (parts[2].split("[")[0].equals(a.name)){
                                        v1 = new Variable(null, a.get(Integer.parseInt(parts[2].split("[")[1].split("]")[0])));
                                        break;
                                    }
                                }
                            }
                        }
                        if (v1 == null) {
                            System.err.println("Error: Cannot find variable with name '" + parts[2] + "' on line " + srcLineNumber);
                            break;
                        }
                        output.addToOutput("1670" + v1.hexAddress);
                        output.addToOutput("1570" + toHex(output.getOutputLineNumber()+1));
                        output.addToOutput("15" + from + "00000");
                    }else {
                        output.addToOutput("1670" + toHex(parts[2]));
                        output.addToOutput("1570" + toHex(output.getOutputLineNumber()+1));
                        output.addToOutput("15" + from + "00000");
                    }
                    break;

                case "rtrg":
                    if(parts[2].equals("r0"))
                        to = "0";
                    if(parts[2].equals("r1"))
                        to = "1";
                    if(parts[2].equals("r2"))
                        to = "2";
                    if(parts[2].equals("r3"))
                        to = "3";
                    if(parts[2].equals("r4"))
                        to = "4";
                    if(parts[2].equals("r5"))
                        to = "5";
                    if(parts[2].equals("r6"))
                        to = "6";
                    if(parts[2].equals("r7"))
                        to = "7";
                    if (!canParse(parts[1])) {
                        for (Variable v : variables) {
                            if (parts[1].equals(v.name)) {
                                v1 = v;
                                break;
                            }
                        }
                        if(v1 == null) {
                            if (parts[1].endsWith("]")) {
                                for (Array a : arrays) {
                                    if (parts[1].split("[")[0].equals(a.name)) {
                                        v1 = new Variable(null, a.get(Integer.parseInt(parts[1].split("[")[1].split("]")[0])));
                                        break;
                                    }
                                }
                            }
                        }
                        if (v1 == null) {
                            System.err.println("Error: Cannot find variable with name '" + parts[1] + "' on line " + srcLineNumber);
                            break;
                        }
                        output.addToOutput("16" + to + "0" + v1.hexAddress);
                    }else {
                        output.addToOutput("16" + to + "0" + toHex(parts[1]));
                    }
                    break;

                //Indirect causes us to read from the ram address stored in the variable given.
                case "rtrgid":
                    if(parts[2].equals("r0"))
                        to = "0";
                    if(parts[2].equals("r1"))
                        to = "1";
                    if(parts[2].equals("r2"))
                        to = "2";
                    if(parts[2].equals("r3"))
                        to = "3";
                    if(parts[2].equals("r4"))
                        to = "4";
                    if(parts[2].equals("r5"))
                        to = "5";
                    if(parts[2].equals("r6"))
                        to = "6";
                    if(parts[2].equals("r7"))
                        to = "7";
                    if (!canParse(parts[1])) {
                        for (Variable v : variables) {
                            if (parts[1].equals(v.name)) {
                                v1 = v;
                                break;
                            }
                        }
                        if(v1 == null) {
                            if (parts[1].endsWith("]")) {
                                for (Array a : arrays) {
                                    if (parts[1].split("[")[0].equals(a.name)) {
                                        v1 = new Variable(null, a.get(Integer.parseInt(parts[1].split("[")[1].split("]")[0])));
                                        break;
                                    }
                                }
                            }
                        }
                        if (v1 == null) {
                            System.err.println("Error: Cannot find variable with name '" + parts[1] + "' on line " + srcLineNumber);
                            break;
                        }
                        output.addToOutput("16" + to + "0" + v1.hexAddress);
                        output.addToOutput("15" + to + "0" + toHex(output.getOutputLineNumber()+1));
                        output.addToOutput("16" + to + "00000");
                    }else {
                        output.addToOutput("16" + to + "0" + toHex(parts[1]));
                        output.addToOutput("15" + to + "0" + toHex(output.getOutputLineNumber()+1));
                        output.addToOutput("16" + to + "00000");
                    }
                    break;

                case "hlt":
                    output.addToOutput("18000000");
                    break;

                case "out":
                    switch (parts[1]) {
                        case "r0":
                            output.addToOutput("17000000");
                            break;
                        case "r1":
                            output.addToOutput("17100000");
                            break;
                        case "r2":
                            output.addToOutput("17200000");
                            break;
                        case "r3":
                            output.addToOutput("17300000");
                            break;
                        case "r4":
                            output.addToOutput("17400000");
                            break;
                        case "r5":
                            output.addToOutput("17500000");
                            break;
                        case "r6":
                            output.addToOutput("17600000");
                            break;
                        case "r7":
                            output.addToOutput("17700000");
                            break;
                    }
                    break;

                case("if"):
                    for (Variable v : variables) {
                        if (parts[1].equals(v.name)) {
                            v1 = v;
                            break;
                        }
                    }
                    if(v1 == null) {
                        if (parts[1].endsWith("]")) {
                            for (Array a : arrays) {
                                if (parts[1].split("[")[0].equals(a.name)) {
                                    v1 = new Variable(null, a.get(Integer.parseInt(parts[1].split("[")[1].split("]")[0])));
                                    break;
                                }
                            }
                        }
                    }
                    if (v1 == null) {
                        System.err.println("Error: Cannot find variable with name '" + parts[1] + "' on line " + srcLineNumber);
                        break;
                    }
                    for (Variable v : variables) {
                        if (parts[3].equals(v.name)) {
                            v2 = v;
                            break;
                        }
                    }
                    if(v1 == null) {
                        if (parts[3].endsWith("]")) {
                            for (Array a : arrays) {
                                if (parts[3].split("[")[0].equals(a.name)) {
                                    v1 = new Variable(null, a.get(Integer.parseInt(parts[3].split("[")[1].split("]")[0])));
                                    break;
                                }
                            }
                        }
                    }
                    if (v2 == null) {
                        System.err.println("Error: Cannot find variable with name '" + parts[3] + "' on line " + srcLineNumber);
                        return;
                    }
                    if(parts[2].equals("==") || parts[2].equals("<=") || parts[2].equals(">=") ) {
                        output.openBranch(new IfStatement(v1, parts[2], v2));
                    }else{
                        System.err.println("Error: Invalid if statement condition '" + parts[2] + "' on line " + srcLineNumber);
                    }
                    break;

                case("while"):
                    String r = null;
                    String constant = null;
                    switch(parts[1]) {
                        default:
                            for (Variable v : variables) {
                                if (parts[1].equals(v.name)) {
                                    v1 = v;
                                    break;
                                }
                            }
                            if (v1 == null) {
                                System.err.println("Error: Cannot find variable with name '" + parts[1] + "' on line " + srcLineNumber);
                                return;
                            }
                            break;
                        case "r0":
                            r = "0";
                            break;
                        case "r1":
                            r = "1";
                            break;
                        case "r2":
                            r = "2";
                            break;
                        case "r3":
                            r = "3";
                            break;
                        case "r4":
                            r = "4";
                            break;
                        case "r5":
                            r = "5";
                            break;
                        case "r6":
                            r = "6";
                            break;
                        case "r7":
                            r = "7";
                            break;
                    }
                    switch(parts[3]) {
                        default:
                            for (Variable v : variables) {
                                if (parts[3].equals(v.name)) {
                                    v2 = v;
                                    break;
                                }
                            }
                            if(v2 != null)
                                break;
                            constant = toHex(parts[3]);
                            if(constant != null)
                                break;
                            if (v2 == null) {
                                System.err.println("Error: Cannot find variable with name '" + parts[3] + "' on line " + srcLineNumber);
                                return;
                            }
                            break;
                        case "r0":
                            r += "0";
                            break;
                        case "r1":
                            r += "1";
                            break;
                        case "r2":
                            r += "2";
                            break;
                        case "r3":
                            r += "3";
                            break;
                        case "r4":
                            r += "4";
                            break;
                        case "r5":
                            r += "5";
                            break;
                        case "r6":
                            r += "6";
                            break;
                        case "r7":
                            r += "7";
                            break;
                    }
                    if(parts[2].equals("==") || parts[2].equals("<=") || parts[2].equals(">=") ) {
                        if(v1 != null && v2 != null)
                            output.openBranch(new WhileLoop(v1, parts[2], v2));
                        else if(v1 != null && constant != null)
                            output.openBranch(new WhileLoop(v1, parts[2], constant));
                        else if(v1 == null && r != null)
                            output.openBranch(new WhileLoop(r, parts[2]));
                        else
                            System.err.println("Error: Invalid while statement on line " + srcLineNumber);
                    }else{
                        System.err.println("Error: Invalid while statement condition '" + parts[2] + "' on line " + srcLineNumber);
                    }
                    break;
            }
        }
    }

    private void formMethodCall(String line) {
        String methodName = line.split("\\(")[0];
        String s = line.split("\\(")[1];

        String[] params = null;
        int numParams;
        if(s.length() == 1)
            numParams = 0;
        else {
            params = line.split("\\(")[1].split("\\)")[0].split(",");

            if (!params[0].trim().isEmpty())
                numParams = params.length;
            else
                numParams = 0;
        }

        Iterator<Method> it = methods.iterator();
        while(it.hasNext()){
            Method m = it.next();
            if(m.name.equals(methodName)){
                if(m.numParameters == numParams){
                    formCallStackPush();
                    formUCJump(m.startPoint.hexPos);
                    return;
                }
            }
        }
        System.err.println("Error: cannot find method: '" + methodName + "' at line no " + srcLineNumber);
    }

    //Makes the program jump back to the address at the top of the call stack.
    //This also pops the top of the stack.
    private void formMethodReturn(){
        output.addToOutput(new String[]{
                "1600febe",//Stack pointer in r0
                "01000101",//Load 1 into r1
                "03010000",//Increment the stack pointer
                "1500febe" //Update the stack pointer
        });
        output.addToOutput(new String[]{
                "1500" + toHex(output.getOutputLineNumber()+1),//Set next lines read address to the stack pointer
                "16000000",//Read the current top of stack
                "1500" + toHex(output.getOutputLineNumber()+3),//Set next lines jump address to the return address
                "11000000"//Jump to the return address
        });
    }

    private void formUCJump(String dest) {
        output.addToOutput("1100" + dest);
    }

    /*STACK DATA--------------------------------------------------------
        Stack sizes:
            register stack = ad. 65211
            call stack     = ad. 65212

        Stack pointers:
            register stack = ad. 65213
            call stack     = ad. 65214
    */

    private void formStackSetup(){
        //Sizes
        output.addToOutput(new String[]{
                "0100ff00",
                "1500febb",
                "01004000",
                "1500febc"
        });

        //Stack Pointers
        output.addToOutput(new String[]{
                "01ffbe00",
                "1500febd",
                "01ffff00",
                "1500febe"
        });
    }

    //Call stack is 64 addresses long from 65471 to 65535
    private void formCallStackPush(){
        int returnDest = output.getOutputLineNumber()+8;

        output.addToOutput("1600febe");  //Stack pointer in r0
        output.addToOutput("01"+toHex(returnDest)+"01"); //Put Return dest in r1

        output.addToOutput("1500" + toHex(output.getOutputLineNumber()+1)); //Set next lines write dest to the stack pointer
        output.addToOutput("15100000"); //Write to the current stack pointer

        output.addToOutput(new String[]{
                "01000101",//Set r1 to 1
                "04010000",//decrement r0 using r1
                "1500febe" //Update the stack pointer
        });

        currentCallStackSize++;
        if(currentCallStackSize > 64){
            System.err.println("Error: call stack overflow!");
        }
    }

    private void formCallStackGet(){

    }

    private void formCallStackPop(){

        currentCallStackSize--;
    }

    //Register Stack is 255 addresses long from 65215 to 65470
    private void formRegStackPush(int regNo){

        currentRegStackSize++;
        if(currentRegStackSize > 255){
            System.err.println("Error: register stack overflow!");
        }
    }

    private void formRegStackGet(){

    }

    private void formRegStackPop(){

        currentRegStackSize--;
    }

    public static boolean canParse(String s){
        try {
            Integer.parseInt(s);
        }catch(NumberFormatException e){
            return false;
        }
        return true;
    }

    public static String toHex(String s){
        int i;
        try {
            i = Integer.parseInt(s);
        }catch(NumberFormatException e){
            System.err.println("Error: Invalid number format of string " + s);
            return null;
        }
        String hex = Integer.toHexString(i);
        if(hex.length() <= 4){
            switch(4 - hex.length()){
                case 0:
                    break;
                case 1:
                    hex = "0" + hex;
                    break;
                case 2:
                    hex = "00" + hex;
                    break;
                case 3:
                    hex = "000" + hex;
                    break;
            }
        }else{
            System.err.println("Error: Cannot create hex string of value " + s + " requiring over 16 bits");
        }
        return hex;
    }

    public static String toHex(int i){
        String hex = Integer.toHexString(i);
        if(hex.length() <= 4){
            switch(4 - hex.length()){
                case 0:
                    break;
                case 1:
                    hex = "0" + hex;
                    break;
                case 2:
                    hex = "00" + hex;
                    break;
                case 3:
                    hex = "000" + hex;
                    break;
            }
        }else{
            System.err.println("Error: Cannot create hex string of value " + i + " requiring over 16 bits");
        }
        return hex;
    }

    public static void main(String[] args){
        try {
            Compiler c = new Compiler();
            c.run();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

}
