package CustomCompiler;

import Shared.OutputWriter;

import java.lang.*;

/**
 * Created by os4g14 on 27/10/14.
 */
public class VariableManager {

    private int startIndex;
    private int stackPointerAddr;
    private int numVariablesAddr;
    private OutputWriter output;

    public VariableManager(int startIndex, OutputWriter output){
        this.startIndex = startIndex;
        this.output = output;
    }

    public void formManagerCode(){
        stackPointerAddr = output.getOutputLineNumber();
        numVariablesAddr = stackPointerAddr = 1;
        output.addToOutput(new String[]{
                "0000" + Compiler.toHex(startIndex), //Variable Stack pointer starting at the startIndex then decreasing addresses
                "00000000", //Number of variables in the current scope
        });
    }

    public void incrScopeLevel(){
        int startOfProc = output.getOutputLineNumber();
        output.addToOutput(new String[]{
                "1600"+ Compiler.toHex(stackPointerAddr),  //Put the variable stack pointer in r0
                "1610"+ Compiler.toHex(numVariablesAddr),//Put the number of variables in the current scope in r1
                "1500"+ Compiler.toHex(startOfProc + 3), //Set the write location of the next line to the current stack pointer
                "1500ffff", //Write the number of variables in the current scope to the stack
                "01000102", //Load 1 into r2
                "03020000", //Increment the variable stack pointer
                "1500"+ Compiler.toHex(stackPointerAddr), //Update the variable stack pointer in memory
                //"1600"+Compiler.toHex(managerCodeLocation+2), //Get the scope depth
                //"03020000" //Increment the scope depth
        });
    }

    public void decrScopeLevel(){
        int startOfProc = output.getOutputLineNumber();
        output.addToOutput(new String[]{
                "1600"+ Compiler.toHex(stackPointerAddr),  //Put the variable stack pointer in r0
                "1610"+ Compiler.toHex(numVariablesAddr),//Put the number of variables in the current scope in r1
                "04010000", //Decrement the variable stack pointer by the number of variables
                "01000102", //Load 1 into r2
                "04020000", //Decrement the variable stack pointer by 1
                "1500"+ Compiler.toHex(stackPointerAddr), //Update the variable stack pointer in memory
                "1500"+ Compiler.toHex(startOfProc + 6), //Set the read location of the next line to the current stack pointer
                "1610ffff", //Read the number of variables in the previous scope into r1
                "1510"+ Compiler.toHex(numVariablesAddr) //Update the number of variables to the previous scope
        });
    }

    public void newVariable(){

    }

    public void newVariable(int value){

    }

    public void newArray(int length){

    }

}
