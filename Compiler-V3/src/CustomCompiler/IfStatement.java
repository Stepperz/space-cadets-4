package CustomCompiler;

import Shared.Branch;

import java.lang.*;

/**
 * Created by os4g14 on 24/10/14.
 */
public class IfStatement extends Branch {

    public Variable c1, c2;
    public String compareFunc;

    public IfStatement(Variable c1, String compareFunc, Variable c2){
        this.c1 = c1;
        this.c2 = c2;
        this.compareFunc = compareFunc;
    }

    @Override
    public void addStartBranchOperations(int startLine){
        this.startLine = startLine;
        addInstruction("1600"+c1.hexAddress);
        addInstruction("1610"+c2.hexAddress);
        addInstruction("IF-STAT.");//Insert jump instruction at startLine
    }

    @Override
    public void addFinalBranchOperations(){
        switch(compareFunc) {
            case "==":
                setInnerInstruction(2, "1201" + Compiler.toHex(startLine + numLines));
                break;
            case "<=":
                setInnerInstruction(2, "1401" + Compiler.toHex(startLine + numLines));
                break;
            case ">=":
                setInnerInstruction(2, "1301" + Compiler.toHex(startLine + numLines));
                break;
        }
    }
}
