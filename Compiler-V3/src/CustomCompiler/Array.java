package CustomCompiler;

/**
 * Created by os4g14 on 27/10/14.
 */
public class Array {

    public String name;
    private int startLocation;
    public int length;

    public Array(String name, int startLocation, int length){
        this.name = name;
        this.startLocation = startLocation;
        this.length = length;
    }

    public String get(int index){
        if(index <= length - 1)
            return Compiler.toHex(startLocation + index);
        System.err.println("Error: Array Index Out of bounds in array '" + name + "'.");
        return null;
    }

}
