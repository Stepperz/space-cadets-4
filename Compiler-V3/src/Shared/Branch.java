package Shared;

import java.util.ArrayList;

/**
 * Created by os4g14 on 24/10/14.
 */
public abstract class Branch {

    public int numLines = 0;
    public int startLine;

    protected ArrayList<String> innerInstructions;

    public Branch(){
        innerInstructions = new ArrayList<>();
    }

    public void setInnerInstruction(int i, String instruction){
        innerInstructions.remove(i);
        innerInstructions.add(i, instruction);
    }

    public void addInstruction(int index, String hexInstruction){
        innerInstructions.add(index, hexInstruction);
        numLines++;
    }

    public void addInstruction(String hexInstruction){
        innerInstructions.add(hexInstruction);
        numLines++;
    }

    public void addInstructions(String[] s){
        for(String out : s){
            innerInstructions.add(out);
        }
        numLines += s.length;
    }

    public void addInnerBranchInstructions(Branch b){
        innerInstructions.addAll(b.getInnerInstructions());
        numLines += b.numLines;
    }

    public ArrayList<String> getInnerInstructions(){
        return innerInstructions;
    }

    public void addStartBranchOperations(int startLine){
        this.startLine = startLine;
    }

    public void addFinalBranchOperations(){

    }

}
