package Shared;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Created by os4g14 on 24/10/14.
 */
public class OutputWriter {

    private PrintWriter writer;
    private ArrayList<String> output;
    private ArrayList<Branch> branches;
    private int outputLineNumber = 0;
    public int branchDepth = 0;


    public OutputWriter(){
        output = new ArrayList<>();
        branches = new ArrayList<>();
    }

    public void setOutput(int i, String s){
        output.remove(i);
        output.add(i, s);
    }

    public int getOutputLineNumber(){
        int r = outputLineNumber;
        for(Branch b : branches){
            r += b.numLines;
        }
        return r;
    }

    public void incrOutputLineNumber(){
        outputLineNumber++;
    }

    public void writeToFile(String fileName){
        try {
            writer = new PrintWriter(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int len = output.size();
        writer.println("v2.0 raw");
        for(int i = 0; i < len; i++){
            writer.println(output.get(i));
        }
        writer.close();
    }

    public void openBranch(Branch b){
        if(!branches.isEmpty()) {
            Branch old = branches.get(branches.size()-1);
            b.addStartBranchOperations(old.numLines + old.startLine);
        }else{
            b.addStartBranchOperations(outputLineNumber);
        }
        branches.add(b);
        branchDepth++;
    }

    public void closeCurrentBranch(){
        Branch b = branches.get(branches.size()-1);
        b.addFinalBranchOperations();
        branches.remove(b);
        if(!branches.isEmpty()) {
            branches.get(branches.size() - 1).addInnerBranchInstructions(b);
        }else{
            output.addAll(b.getInnerInstructions());
            outputLineNumber += b.numLines;
        }
        branchDepth--;
    }

    public void addToOutput(int index, String s){
        output.add(index, s);
        outputLineNumber++;
    }

    public void addToOutput(String s){
        if(branches.size() > 0) {
            branches.get(branches.size()-1).addInstruction(s);
        }else{
            output.add(s);
            outputLineNumber++;
        }
    }

    public void addToOutput(String[] s){
        if(branches.size() > 0) {
            branches.get(branches.size()-1).addInstructions(s);
        }else{
            for(String out : s) {
                output.add(out);
            }
            outputLineNumber += s.length;
        }
    }

}
