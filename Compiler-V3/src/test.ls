run(){
    while test1 <= loops {
        test1 = test1 + 1;
        out r0;
        if test1 == loops {
            ldi r0 31;
            out r0;
        }
        if test1 == test2 {
            ldi r0 1023;
            out r0;
        }
    }
}

main(){
    svar test1 0;
    svar test2 9;
    svar loops 32;

    array testArray 10;
    testArray[0] = 64;

    run();

	hlt;
}