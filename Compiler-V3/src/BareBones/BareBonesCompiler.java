package BareBones;

import Shared.Branch;
import Shared.OutputWriter;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Ollie on 29/10/2014.
 */
public class BareBonesCompiler {

    int srcLineNumber = 1;
    String[] parts;

    OutputWriter output;

    ArrayList<BBVariable> variables;
    ArrayList<Branch> branches;

    String fileName = "NULL";

    public BareBonesCompiler() throws FileNotFoundException {
        fileName = JOptionPane.showInputDialog("Enter Bare Bones File Name To Compile...");

        parts = new String[4];

        output = new OutputWriter();
        variables = new ArrayList<>();
        branches = new ArrayList<>();
    }

    //Called to start the compiler running
    public void run() throws IOException {
        if(fileName == null)
            return;

        variablePass();
        mainPass();

        output.writeToFile(fileName + ".txt");

        JOptionPane.showMessageDialog(null, "Compilation Complete");
    }

    private void mainPass() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("src/" + fileName + ".bb"));
        String line;
        srcLineNumber = 1;

        while((line = reader.readLine()) != null) {
            line = line.trim();
            if (!line.startsWith("//")) {
                if (!line.equals("end;")) {
                    processInstruction(line);
                } else if (output.branchDepth > 0) {
                    output.closeCurrentBranch();
                }
            }
            srcLineNumber++;
        }

        reader.close();
    }

    //Variables populate from ad.65210 and decreases to lower addresses.
    //There is no checking for overwriting a program so be aware of that.
    //Variables are not created at runtime only given pre designated memory addresses.
    private void variablePass() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("src/" + fileName + ".bb"));
        String line;
        String[] parts;
        srcLineNumber = 1;
        int currentAddress = 65210;

        while((line = reader.readLine()) != null){
            line = line.trim();
            if(line.startsWith("clear") || line.startsWith("incr") || line.startsWith("decr") || line.startsWith("while")) {
                if(!line.endsWith(";")) {
                    System.err.println("Error: Expected ';' not found on line " + srcLineNumber);
                    System.exit(-1);
                }
                parts = line.split(";")[0].split(" ");
                if(parts.length == 2){
                    boolean added = false;
                    BBVariable v = new BBVariable(parts[1], toHex(currentAddress));
                    for(BBVariable b : variables){
                        if(b.name.equals(parts[1]))
                            added = true;
                    }
                    if(!added) {
                        variables.add(v);
                        addVariableInitialisation(v);
                        currentAddress--;
                    }
                }else{
                    System.err.println("Error: Variable cannot be initialised at line " + srcLineNumber);
                }
            }
            srcLineNumber++;
        }

        reader.close();
    }

    //Adds variable initialisation code to the top of the file.
    private void addVariableInitialisation(BBVariable v){
        output.addToOutput("01000000");
        output.addToOutput("1500" + v.hexAddress);
    }

    private void processInstruction(String line){
        if(!line.isEmpty()) {
            if (!line.endsWith(";")) {
                System.err.println("Error: missing ';' at line " + srcLineNumber);
                return;
            }

            line = line.split(";")[0];

            parts = line.split(" ");
            BBVariable v1 = null;

            String s;
            switch (parts[0]) {
                case "clear":
                    for(BBVariable v : variables){
                        if(v.name.equals(parts[1])){
                            v1 = v;
                        }
                    }
                    output.addToOutput("01000000");
                    output.addToOutput("1500" + v1.hexAddress);
                    break;

                case "incr":
                    for(BBVariable v : variables){
                        if(v.name.equals(parts[1])){
                            v1 = v;
                        }
                    }
                    output.addToOutput("01000100");
                    output.addToOutput("1610" + v1.hexAddress);
                    output.addToOutput("03010000");
                    output.addToOutput("1500" + v1.hexAddress);
                    break;

                case "decr":
                    for(BBVariable v : variables){
                        if(v.name.equals(parts[1])){
                            v1 = v;
                        }
                    }
                    output.addToOutput("01000100");
                    output.addToOutput("1610" + v1.hexAddress);
                    output.addToOutput("04100000");
                    output.addToOutput("1500" + v1.hexAddress);
                    break;

                case "hlt":
                    output.addToOutput("18000000");
                    break;

                case "out":
                    for(BBVariable v : variables){
                        if(v.name.equals(parts[1])){
                            v1 = v;
                        }
                    }
                    output.addToOutput("1600" + v1.hexAddress);
                    output.addToOutput("17000000");
                    break;

                case("while"):
                    if(parts[2].equals("not") || parts[4].equals("do")) {
                        for (BBVariable v : variables) {
                            if (parts[1].equals(v.name)) {
                                v1 = v;
                                break;
                            }
                        }
                        String constant = toHex(parts[3]);
                        output.openBranch(new BBWhileLoop(v1, constant));
                    }
                    break;
            }
        }
    }

    public static boolean canParse(String s){
        try {
            Integer.parseInt(s);
        }catch(NumberFormatException e){
            return false;
        }
        return true;
    }

    public static String toHex(String s){
        int i;
        try {
            i = Integer.parseInt(s);
        }catch(NumberFormatException e){
            System.err.println("Error: Invalid number format of string " + s);
            return null;
        }
        String hex = Integer.toHexString(i);
        if(hex.length() <= 4){
            switch(4 - hex.length()){
                case 0:
                    break;
                case 1:
                    hex = "0" + hex;
                    break;
                case 2:
                    hex = "00" + hex;
                    break;
                case 3:
                    hex = "000" + hex;
                    break;
            }
        }else{
            System.err.println("Error: Cannot create hex string of value " + s + " requiring over 16 bits");
        }
        return hex;
    }

    public static String toHex(int i){
        String hex = Integer.toHexString(i);
        if(hex.length() <= 4){
            switch(4 - hex.length()){
                case 0:
                    break;
                case 1:
                    hex = "0" + hex;
                    break;
                case 2:
                    hex = "00" + hex;
                    break;
                case 3:
                    hex = "000" + hex;
                    break;
            }
        }else{
            System.err.println("Error: Cannot create hex string of value " + i + " requiring over 16 bits");
        }
        return hex;
    }

    public static void main(String[] args){
        try {
            BareBonesCompiler c = new BareBonesCompiler();
            c.run();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

}
