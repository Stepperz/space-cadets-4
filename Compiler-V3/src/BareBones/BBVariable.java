package BareBones;

/**
 * Created by Ollie on 29/10/2014.
 */
public class BBVariable {

    public String name;
    public String hexAddress;

    public BBVariable(String name, String hexAddress){
        this.name = name;
        this.hexAddress = hexAddress;
    }

}
