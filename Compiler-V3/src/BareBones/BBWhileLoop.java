package BareBones;

import Shared.Branch;

/**
 * Created by Ollie on 25/10/2014.
 */
public class BBWhileLoop extends Branch{

    public BBVariable c1;
    public String constant;

    public BBWhileLoop(BBVariable c1, String constant){
        this.c1 = c1;
        this.constant = constant;
    }

    @Override
    public void addStartBranchOperations(int startLine){
        this.startLine = startLine;
        addInstruction("1600"+c1.hexAddress);
        addInstruction("01"+constant+"01");
        addInstruction("WHILE");//Insert jump instruction at startLine
    }

    @Override
    public void addFinalBranchOperations(){
        addInstruction("1100" + CustomCompiler.Compiler.toHex(startLine));
        setInnerInstruction(2, "1201" + CustomCompiler.Compiler.toHex(startLine + numLines));
    }
}
