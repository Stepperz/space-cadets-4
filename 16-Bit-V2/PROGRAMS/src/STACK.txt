//STACK VARIABLES----------------------------------
//STACK SIZE(BYTES)(50000)
LDR r0 255
RGTR r0 50000
//CURRENT STACK POINTER(50001)
LDR r0 65535
RGTR r0 50001

//#Program Start#
JMP UC start#

//STACK FUNCTIONS----------------------------------

[StackPush#]
//PUSH #(JMP ID = 5)#
//@params (r0-DATA, r7-RETURN ADDR.)
RTRG 50001 r1
RGTR r1 7
RGTR r0 10000
LDR r0 1
SUB 1 0 1
RGTR r1 50001
RGTR r7 12
JMP UC 10000

[StackPop#]
//POP #(JMP ID = 13)#
//@params (r7-RETURN ADDR.)
RTRG 50001 r0
LDR r1 1
ADD 0 1 0
RGTR r0 50001
RGTR r7 18
JMP UC 10000

[StackGet#]
//GET #(JMP ID = 19)#
//@params (r6-DATA DEST., r7-RETURN ADDR.)
RTRG 50001 r2
LDR r0 1
ADD 2 0 2
RGTR r2 23
RTRG 10000 r3
RGTR r6 25
RGTR r3 10000
RGTR r7 27
JMP UC 10000

//Test Program #(JMP ID = 28)#---------------------
[start#]

//Add 1234 to stack
LDR r0 1234
LDR r7 31
JMP UC StackPush#

//Add 255 to stack
LDR r0 255
LDR r7 34
JMP UC StackPush#

//Output the current stack data
LDR r6 255
LDR r7 37
JMP UC StackGet#
RTRG 255 r0
OUT r0

//POP
LDR r7 41
JMP UC StackPop#

//Finish Program
JMP UC start#