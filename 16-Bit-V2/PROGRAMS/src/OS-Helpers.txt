JMP UC InitStack#

//Helper Methods (Fixed Functions!!!)=============================

//Initialises the Stack------------------
[InitStack#]
//Max Stack Size (a50000)
LDR r0 32
RGTR r0 50000
//Current Stack Pointer (a50001)
LDR r0 65535
RGTR r0 50001
JMP UC Start#

//Stack Push Function(line-6)--------------------
//@params(r0-Data, r7-Return Addr.)
[StackPush#]
RTRG 50001 r1
RGTR r1 8
RGTR r0 10000
LDR r0 1
SUB 1 0 1

//Overflow check
RTRG 50000 r4
LDR r5 65535
SUB 5 4 4
JMP A<B 1 4 StackOverflow#

RGTR r1 50001
RGTR r7 17
JMP UC 10000

//Stack Pop Function(line-18)---------------------
//@params(r7-Return Addr.)
[StackPop#]
RTRG 50001 r0
LDR r1 65535
JMP A==B 0 1 skipPop#
LDR r1 1
ADD 0 1 0
RGTR r0 50001
[skipPop#]
RGTR r7 26
JMP UC 10000

//Stack Overflow Error(line-27)-------------------
[StackOverflow#]
LDR r0 65535
exit#

//Exit Program Function(line-29)------------------
//@params(r0-Exit Id)
[exit#]
OUT r0
HLT

//================================================================

//Main Program(line-31)
[Start#]
LDR r0 100
LDR r7 34
JMP UC StackPush#
LDR r0 200
LDR r7 37
JMP UC StackPush#
LDR r7 39
JMP UC StackPop#
LDR r0 100
LDR r7 42
JMP UC StackPush#
JMP UC Start#

[countLoop#]
LDR r7 255
LDR r0 0
[loop#]
LDR r1 1
ADD 0 1 0
OUT r0
JMP A==B 0 7 Start#
JMP UC loop#