package programmer.v2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class ProgrammerV2 {
    
    boolean finished;
    int lineNo;
    String from;
    String to;
    String[] parts;
    PrintWriter writer;
    List<String> output;
    
    int jumpPassLine = 0;
    ArrayList<JumpPoint> jumpPoints = new ArrayList<>();
    
    String fileName = "NULL";
    
    public ProgrammerV2() throws IOException{
        fileName = JOptionPane.showInputDialog("Enter File Name To Compile...");
        
        finished = false;
        lineNo = 0;
        parts = new String[4];
        writer = new PrintWriter(fileName + ".txt");
        output = new ArrayList<>();
        
        BufferedReader reader = new BufferedReader(new FileReader("src/" + fileName + ".txt"));
        String line;
        
        while((line = reader.readLine()) != null){
            jumpPass(line);
            if(!line.startsWith("//") && !line.isEmpty() && !line.startsWith("[")){
                jumpPassLine++;
            }
        }
        reader.close();
        reader = new BufferedReader(new FileReader("src/" + fileName + ".txt"));
        
        while((line = reader.readLine()) != null){
            decode(line);
        }
        
        int len = output.size();
        writer.println("v2.0 raw");
        for(int i = 0; i < len; i++){
            writer.println(output.get(i));
        }
        writer.close();
    }
    
    public static void main(String[] args) {
        try {
            ProgrammerV2 bp = new ProgrammerV2();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public void decode(String in){
        parts = in.split(" ");
        
        switch(parts[0]){
            case "//":
                break;
        
            case "DATA":
                switch(toHex(parts[1]).length()){
                    case 1:
                        output.add("0000000" + toHex(parts[1]));
                        break;
                    case 2:
                        output.add("000000" + toHex(parts[1]));
                        break;
                    case 3:
                        output.add("00000" + toHex(parts[1]));
                        break;
                    case 4:
                        output.add("0000" + toHex(parts[1]));
                        break;
                }
                break;

            case "LDR":
                if(parts[1].equals("r0")){
                    String s = toHex(parts[2]);
                    if(s.length() <= 4){
                        switch(4 - s.length()){
                            case 0:
                                output.add("01" + s + "00");
                                break;
                            case 1:
                                output.add("010" + s + "00");
                                break;
                            case 2:
                                output.add("0100" + s + "00");
                                break;
                            case 3:
                                output.add("01000" + s + "00");
                                break;
                        }
                    }          
                }
                if(parts[1].equals("r1")){
                    String s = toHex(parts[2]);
                    if(s.length() <= 4){
                        switch(4 - s.length()){
                            case 0:
                                output.add("01" + s + "01");
                                break;
                            case 1:
                                output.add("010" + s + "01");
                                break;
                            case 2:
                                output.add("0100" + s + "01");
                                break;
                            case 3:
                                output.add("01000" + s + "01");
                                break;
                        }
                    }          
                }
                if(parts[1].equals("r2")){
                    String s = toHex(parts[2]);
                    if(s.length() <= 4){
                        switch(4 - s.length()){
                            case 0:
                                output.add("01" + s + "02");
                                break;
                            case 1:
                                output.add("010" + s + "02");
                                break;
                            case 2:
                                output.add("0100" + s + "02");
                                break;
                            case 3:
                                output.add("01000" + s + "02");
                                break;
                        }
                    }          
                }
                if(parts[1].equals("r3")){
                    String s = toHex(parts[2]);
                    if(s.length() <= 4){
                        switch(4 - s.length()){
                            case 0:
                                output.add("01" + s + "03");
                                break;
                            case 1:
                                output.add("010" + s + "03");
                                break;
                            case 2:
                                output.add("0100" + s + "03");
                                break;
                            case 3:
                                output.add("01000" + s + "03");
                                break;
                        }
                    }          
                }
                if(parts[1].equals("r4")){
                    String s = toHex(parts[2]);
                    if(s.length() <= 4){
                        switch(4 - s.length()){
                            case 0:
                                output.add("01" + s + "04");
                                break;
                            case 1:
                                output.add("010" + s + "04");
                                break;
                            case 2:
                                output.add("0100" + s + "04");
                                break;
                            case 3:
                                output.add("01000" + s + "04");
                                break;
                        }
                    }          
                }
                if(parts[1].equals("r5")){
                    String s = toHex(parts[2]);
                    if(s.length() <= 4){
                        switch(4 - s.length()){
                            case 0:
                                output.add("01" + s + "05");
                                break;
                            case 1:
                                output.add("010" + s + "05");
                                break;
                            case 2:
                                output.add("0100" + s + "05");
                                break;
                            case 3:
                                output.add("01000" + s + "05");
                                break;
                        }
                    }          
                }
                if(parts[1].equals("r6")){
                    String s = toHex(parts[2]);
                    if(s.length() <= 4){
                        switch(4 - s.length()){
                            case 0:
                                output.add("01" + s + "06");
                                break;
                            case 1:
                                output.add("010" + s + "06");
                                break;
                            case 2:
                                output.add("0100" + s + "06");
                                break;
                            case 3:
                                output.add("01000" + s + "06");
                                break;
                        }
                    }          
                }
                if(parts[1].equals("r7")){
                    String s = toHex(parts[2]);
                    if(s.length() <= 4){
                        switch(4 - s.length()){
                            case 0:
                                output.add("01" + s + "07");
                                break;
                            case 1:
                                output.add("010" + s + "07");
                                break;
                            case 2:
                                output.add("0100" + s + "07");
                                break;
                            case 3:
                                output.add("01000" + s + "07");
                                break;
                        }
                    }          
                }
                break;
            
            case "MVR":
                if(parts[1].equals("r0"))
                    from = "0";
                if(parts[1].equals("r1"))
                    from = "1";
                if(parts[1].equals("r2"))
                    from = "2";
                if(parts[1].equals("r3"))
                    from = "3";
                if(parts[1].equals("r4"))
                    from = "4";
                if(parts[1].equals("r5"))
                    from = "5";
                if(parts[1].equals("r6"))
                    from = "6";
                if(parts[1].equals("r7"))
                    from = "7";
                if(parts[2].equals("r0"))
                    to = "0";
                if(parts[2].equals("r1"))
                    to = "1";
                if(parts[2].equals("r2"))
                    to = "2";
                if(parts[2].equals("r3"))
                    to = "3";
                if(parts[2].equals("r4"))
                    to = "4";
                if(parts[2].equals("r5"))
                    to = "5";
                if(parts[2].equals("r6"))
                    to = "6";
                if(parts[2].equals("r7"))
                    to = "7";
                output.add(("020000" + to + from));
                break;
            case "ADD":
                output.add("03" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));
                break;
            case "SUB":
                output.add("04" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));   
                break;
            case "MUL":
                break;
            case "DIV":
                output.add("06" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));
                break;
            case "NEG":
                output.add("07" + toHex(parts[1]) + "0000" + toHex(parts[2]));
                break;
            case "SFT":
                output.add("08" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));
                break;
            case "IVA":
                output.add("09" + toHex(parts[1]) + "0000" + toHex(parts[2]));
                break;
            case "IVB":
                output.add("0a" + toHex(parts[1]) + "0000" + toHex(parts[2]));
                break;
            case "AND":
                output.add("0b" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));
                break;
            case "NAND":
                output.add("0c" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));
                break;
            case "OR":
                output.add("0d" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));
                break;
            case "NOR":
                output.add("0e" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));
                break;
            case "XOR":
                output.add("0f" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));
                break;
            case "XNOR":
                output.add("10" + toHex(parts[1]) + toHex(parts[2]) + "000" + toHex(parts[3]));
                break;
            case "JMP":
                String s;
                switch(parts[1]){
                    case "UC":
                        s = parts[2];
                        if(s.endsWith("#")){
                            for(JumpPoint p : jumpPoints){
                                if(p.name.equals(s))
                                    output.add("1100" + p.hexPos);
                            }
                        }else{
                            s = toHex(parts[2]);
                            if(s.length() <= 4){
                                switch(4 - s.length()){
                                    case 0:
                                        output.add("1100" + s);
                                        break;
                                    case 1:
                                        output.add("11000" + s);
                                        break;
                                    case 2:
                                        output.add("110000" + s);
                                        break;
                                    case 3:
                                        output.add("1100000" + s );
                                        break;
                                }
                            }
                        }
                        break;
                    case "A==B":
                        s = parts[4];
                        if(s.endsWith("#")){
                            for(JumpPoint p : jumpPoints){
                                if(p.name.equals(s))
                                    output.add("12" + toHex(parts[2]) + toHex(parts[3]) + p.hexPos);
                            }
                        }else{
                            s = toHex(parts[4]);
                            if(s.length() <= 4){
                                switch(4 - s.length()){
                                    case 0:
                                        output.add("12" + toHex(parts[2]) + toHex(parts[3]) + toHex(parts[4]));
                                        break;
                                    case 1:
                                        output.add("12" + toHex(parts[2]) + toHex(parts[3]) + "0" + toHex(parts[4]));
                                        break;
                                    case 2:
                                        output.add("12" + toHex(parts[2]) + toHex(parts[3]) + "00" + toHex(parts[4]));
                                        break;
                                    case 3:
                                        output.add("12" + toHex(parts[2]) + toHex(parts[3]) + "000" + toHex(parts[4]));
                                        break;
                                }
                            } 
                        }
                        break;
                    case "A<B":
                        s = parts[4];
                        if(s.endsWith("#")){
                            for(JumpPoint p : jumpPoints){
                                if(p.name.equals(s))
                                    output.add("13" + toHex(parts[2]) + toHex(parts[3]) + p.hexPos);
                            }
                        }else{
                            s = toHex(parts[4]);
                            if(s.length() <= 4){
                                switch(4 - s.length()){
                                    case 0:
                                        output.add("13" + toHex(parts[2]) + toHex(parts[3]) + toHex(parts[4]));
                                        break;
                                    case 1:
                                        output.add("13" + toHex(parts[2]) + toHex(parts[3]) + "0" + toHex(parts[4]));
                                        break;
                                    case 2:
                                        output.add("13" + toHex(parts[2]) + toHex(parts[3]) + "00" + toHex(parts[4]));
                                        break;
                                    case 3:
                                        output.add("13" + toHex(parts[2]) + toHex(parts[3]) + "000" + toHex(parts[4]));
                                        break;
                                }
                            } 
                        }
                        break;
                    case "A>B":
                        s = parts[4];
                        if(s.endsWith("#")){
                            for(JumpPoint p : jumpPoints){
                                if(p.name.equals(s))
                                    output.add("14" + toHex(parts[2]) + toHex(parts[3]) + p.hexPos);
                            }
                        }else{
                            s = toHex(parts[4]);
                            if(s.length() < 4){
                                switch(4 - s.length()){
                                    case 0:
                                        output.add("14" + toHex(parts[2]) + toHex(parts[3]) + toHex(parts[4]));
                                        break;
                                    case 1:
                                        output.add("14" + toHex(parts[2]) + toHex(parts[3]) + "0" + toHex(parts[4]));
                                        break;
                                    case 2:
                                        output.add("14" + toHex(parts[2]) + toHex(parts[3]) + "00" + toHex(parts[4]));
                                        break;
                                    case 3:
                                        output.add("14" + toHex(parts[2]) + toHex(parts[3]) + "000" + toHex(parts[4]));
                                        break;
                                }
                            } 
                        }
                        break;
                }
                break;
            case "RGTR":
                if(parts[1].equals("r0"))
                    from = "0";
                if(parts[1].equals("r1"))
                    from = "1";
                if(parts[1].equals("r2"))
                    from = "2";
                if(parts[1].equals("r3"))
                    from = "3";
                if(parts[1].equals("r4"))
                    from = "4";
                if(parts[1].equals("r5"))
                    from = "5";
                if(parts[1].equals("r6"))
                    from = "6";
                if(parts[1].equals("r7"))
                    from = "7";
                String st = toHex(parts[2]);
                if(st.length() <= 4){
                    switch(4 - st.length()){
                        case 0:
                            output.add("15" + from + "0" + st);
                            break;
                        case 1:
                            output.add("15" + from + "00" + st);
                            break;
                        case 2:
                            output.add("15" + from + "000" + st);
                            break;
                        case 3:
                            output.add("15" + from + "0000" + st);
                            break;
                    }
                } 
                break;
            case "RTRG":
                if(parts[2].equals("r0"))
                    to = "0";
                if(parts[2].equals("r1"))
                    to = "1";
                if(parts[2].equals("r2"))
                    to = "2";
                if(parts[2].equals("r3"))
                    to = "3";
                if(parts[2].equals("r4"))
                    to = "4";
                if(parts[2].equals("r5"))
                    to = "5";
                if(parts[2].equals("r6"))
                    to = "6";
                if(parts[2].equals("r7"))
                    to = "7";
                String sr = toHex(parts[1]);
                if(sr.length() <= 4){
                    switch(4 - sr.length()){
                        case 0:
                            output.add("16" + to + "0" + sr);
                            break;
                        case 1:
                            output.add("16" + to + "00" + sr);
                            break;
                        case 2:
                            output.add("16" + to + "000" + sr);
                            break;
                        case 3:
                            output.add("16" + to + "0000" + sr);
                            break;
                    }
                } 
                break;
            case "HLT":
                output.add("18000000");
                break;
            case "OUT":
                switch(parts[1]){
                    case "r0":
                        output.add("17000000");
                        break;
                    case "r1":
                        output.add("17100000");
                        break;
                    case "r2":
                        output.add("17200000");
                        break;
                    case "r3":
                        output.add("17300000");
                        break;
                    case "r4":
                        output.add("17400000");
                        break;
                    case "r5":
                        output.add("17500000");
                        break;
                    case "r6":
                        output.add("17600000");
                        break;
                    case "r7":
                        output.add("17700000");
                        break;
                }
                break;
        }
    }
    
    public static String toHex(String s){
        int i = Integer.parseInt(s);
        return Integer.toHexString(i);
    }

    private void jumpPass(String line) {
        if(line.startsWith("[") && line.endsWith("]")){
            jumpPoints.add(new JumpPoint(line.substring(1, line.length()-1), Integer.toString(jumpPassLine)));
        }
    }
}