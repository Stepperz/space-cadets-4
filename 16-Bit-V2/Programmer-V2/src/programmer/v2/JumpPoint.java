package programmer.v2;

public class JumpPoint 
{   
    public String name;
    public String hexPos;
    
    public JumpPoint(String name, String pos){
        this.name = name;
        this.hexPos = ProgrammerV2.toHex(pos);
        if(hexPos.length() < 4){
            switch(4 - hexPos.length()){
                case 1:
                    hexPos = "0" + hexPos;
                    break;
                case 2:
                    hexPos = "00" + hexPos;
                    break;
                case 3:
                    hexPos = "000" + hexPos;
                    break;
            }
        }
    }
}
